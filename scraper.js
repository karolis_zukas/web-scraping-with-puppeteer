const puppeteer = require('puppeteer');

const userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36';
const carMaker = process.argv[2];
const carModel = process.argv[3];

let store = {
  itemsCrawled: 0,
  pagesCrawled: 0,
  averagePrice: 0,
  scrapedPrices: []
};

const url = `https://autogidas.lt/skelbimai/automobiliai/?f_1=${carMaker}&f_model_14=${carModel}&f_216=&f_216=&f_61=&f_42=&f_3=&f_2=&f_376=`;

const scraperArgs = {
  headless: false
};

(async () => {
    const browser = await puppeteer.launch(scraperArgs);
    const page = await browser.newPage();
    page.setUserAgent(userAgent);

    await page.goto(`${url}`);

    const numberOfPages = await page.evaluate(
      () => document.querySelector('.paginator > a:nth-last-child(3) > div').innerHTML
    );


      for (let currentPage = 0; currentPage < numberOfPages; currentPage++) {
        let prices = await page.evaluate(
          () => [...document.querySelectorAll('.item-price > meta:first-child')]
                .map(element => {
                    return element.getAttribute('content');
                })
        );
        store = storeMethods.updateScrapedPrices(prices);
        prices = [];
        store = storeMethods.calculateAverageCost();
        store = storeMethods.updateItemsCrawled();

        if (await page.$('.page.last') !== null) {
          await page.click( '.page.last' );
          store = storeMethods.updatePagesCrawled();
        }
      }

    console.log(store);
    await browser.close();
  })();

  let storeMethods = {
    updatePagesCrawled: () => {
      let newStore = {...store};
      ++newStore.pagesCrawled;
      return newStore;
    },
    updateScrapedPrices: (newPrices) => {
      let newState = {...store};
      newState.scrapedPrices.push(...newPrices);
      return newState;
    },
    calculateAverageCost: () => {
      let newState = {...store};
      let total = 0;
      newState.scrapedPrices.map(price => total += parseInt(price, 10));
      newState.averagePrice = total / newState.scrapedPrices.length;
      return newState;
    },
    updateItemsCrawled: () => {
      let newState = {...store};
      newState.itemsCrawled = newState.scrapedPrices.length;
      return newState;
    }
  }



  